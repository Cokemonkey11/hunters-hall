Bench Bend
By imforfun

Description:
This is one of the models made for my entry to the Melee map Contest #1, enjoy and edit however you wish, but please give credits if used! : )

(All garden models share the same texture!)

Update: texture quality/size reduced (49 kb).

Original Texture (89 kb): [ATTACH]140927[/ATTACH]

Textures:
GardenProps1.blp

Please give me credit for my work
Do not redistribute this model without consent!

Model was uploaded 2014, November 11
Model was last updated 2014, November 11


Visit http://www.hiveworkshop.com for more downloads