Oak (variation 3)
By Fingolfin

Description:
I made this lowpoly tree set some time ago, but never got around animating it, so it was never uploaded. The texture is partially borrowed from the amazing open-source RTS &quot;0 a.d&quot; which i recommend you all to try out. Includes a full animation set which also features a swaying stand sequence. Enjoy!

Textures:
NewOak.blp

Please give me credit for my work
Do not redistribute this model without consent!

Model was uploaded 2014, July 28
Model was last updated 2014, July 28


Visit http://www.hiveworkshop.com for more downloads