package UnitsApi
    import Table
    import PlayersApi
    import DummyUnitStack

    class LightningBallDat
        unit u
        effect fx
        thistype prev = null
        thistype next = null


    class BeastLightningDat
        unit u
        integer ticksUntilNextCheck
        integer balls
        real phase = 0.
        LightningBallDat ballHead = null
        integer typ
        thistype prev = null
        thistype next = null


    public class Units
        private static constant string LIGHTNING_BALL_FX = "Abilities\\Weapons\\FarseerMissile\\FarseerMissile.mdl"

        private static constant real CLOCK_PERIOD           =   1. / 30.
        private static constant real LIGHTNING_CHECK_PERIOD =   5.
        private static constant real BALL_OFF           = 64.
        private static constant real PHASE_ANGULAR_VELOCITY = bj_PI / 8. * CLOCK_PERIOD
        private static constant real EFFECT_SCALE           =   .5
        private static constant real EFFECT_HEIGHT          = 60.

        private static constant integer BLINK_PASSIVE_ID    = 'A00E'
        private static constant integer BLINK_ID            = 'A001'
        private static constant integer HEMISECT_PASSIVE_ID = 'A00H'
        private static constant integer HEMISECT_ID         = 'A00G'
        private static constant integer CONCAT_PASSIVE_ID   = 'A00J'
        private static constant integer CONCAT_ID           = 'A00K'

        static constant integer HUNTER_ID              = 'H00L'
        static constant integer BEAST_ID             = 'H00M'
        static constant integer LESSER_BEAST_ID      = 'H001'
        static constant integer HUNTER_PROJECTILE_ID = 'h000'
        static constant integer ARTIFACT_ID          = 'I000'
        static constant integer GAS_ACTION           = 'I003'

        static constant integer INITIAL_BALLS        = 0
        static constant integer MAX_BALLS             = 5
        static constant integer MAX_BALLS_LESSER     = 3


        private static group grp = CreateGroup()
        private static timer clock = CreateTimer()
        private static BeastLightningDat beastHead = null
        private static Table fromBeast = new Table()

        static unit array hero
        static unit array subHero

        static Table thunderCd    = new Table() // cooldown for AI
        static Table thunderClock = new Table() // clock for max duration
        static Table beastState   = new Table()
        static Table sitePref     = new Table() // A/B preference for Hunter AI

        static item artifact
        static item artifact2

        private static function pushBall(unit u)
            string fxString = LIGHTNING_BALL_FX
            BeastLightningDat ua = fromBeast.loadInt(u.getHandleId()) castTo BeastLightningDat
            LightningBallDat  lbd = new LightningBallDat
            LightningBallDat  swap

            lbd.u = DummyUnitStack.get()
            SetUnitFlyHeight(lbd.u, EFFECT_HEIGHT, 0.)
            SetUnitScale(lbd.u, EFFECT_SCALE, EFFECT_SCALE, EFFECT_SCALE)

            // risky removal of effect
            if localPlayer != GetOwningPlayer(u)
                fxString = ""

            lbd.fx = AddSpecialEffectTarget(fxString, lbd.u, "origin")
            ua.balls++

            if ua.balls == 1

                // beast received its first ball
                UnitRemoveAbility(ua.u, BLINK_PASSIVE_ID)
                UnitAddAbility(   ua.u, BLINK_ID)

            // beast received its third ball
            else if ua.balls == 3 and ua.typ == BEAST_ID
                UnitRemoveAbility(ua.u, HEMISECT_PASSIVE_ID)
                UnitAddAbility(   ua.u, HEMISECT_ID)
            else if ua.balls == 3 and ua.typ == LESSER_BEAST_ID
                UnitRemoveAbility(ua.u, CONCAT_PASSIVE_ID)
                UnitAddAbility(   ua.u, CONCAT_ID)


            if (ua.ballHead castTo integer) == 0
                ua.ballHead = lbd
            else
                swap = ua.ballHead
                swap.prev = lbd
                lbd.next = swap
                ua.ballHead = lbd



        static function countBalls(unit u) returns integer
            BeastLightningDat ua
            if fromBeast.hasInt(u.getHandleId())
                ua = fromBeast.loadInt(u.getHandleId()) castTo BeastLightningDat
                return ua.balls

            BJDebugMsg("Units.countBalls: Tried to count balls on unindexed unit")
            return -1


        static function dischargeBall(unit u)
            BeastLightningDat ua  = fromBeast.loadInt(u.getHandleId()) castTo BeastLightningDat
            LightningBallDat  lbd = ua.ballHead
            LightningBallDat  swap

            DestroyEffect(lbd.fx)
            DummyUnitStack.release(lbd.u)
            swap = lbd.next
            destroy lbd
            ua.ballHead = swap
            ua.balls--

            // beast lost its third ball
            if ua.balls == 2 and ua.typ == BEAST_ID
                UnitRemoveAbility(ua.u, HEMISECT_ID)
                UnitAddAbility(   ua.u, HEMISECT_PASSIVE_ID)
            else if ua.balls == 2 and ua.typ == LESSER_BEAST_ID
                UnitRemoveAbility(ua.u, CONCAT_ID)
                UnitAddAbility(   ua.u, CONCAT_PASSIVE_ID)


            if (swap castTo int) == 0

                // beast no longer has any lightning balls
                UnitRemoveAbility(ua.u, BLINK_ID)
                UnitAddAbility(   ua.u, BLINK_PASSIVE_ID)
            else
                swap.prev = null



        private static function beastLightningTick()
            BeastLightningDat ua = beastHead
            BeastLightningDat uaSwap
            LightningBallDat lbd
            LightningBallDat swap
            integer index
            real facing

            while ua != (null)
                ua.ticksUntilNextCheck--
                facing = GetUnitFacing(ua.u) * bj_DEGTORAD
                ua.phase = ua.phase + PHASE_ANGULAR_VELOCITY

                if ua.ticksUntilNextCheck <= 0

                    // push lightning ball if applicable
                    if ua.balls < MAX_BALLS and ua.typ == BEAST_ID
                        pushBall(ua.u)
                    else if ua.balls < MAX_BALLS_LESSER and ua.typ == LESSER_BEAST_ID
                        pushBall(ua.u)


                    ua.ticksUntilNextCheck = R2I(LIGHTNING_CHECK_PERIOD / CLOCK_PERIOD)


                // update all ball locations
                index = 0
                lbd = ua.ballHead
                while index < ua.balls

                    SetUnitX(lbd.u, GetUnitX(ua.u) + BALL_OFF* Cos(facing + ua.phase + index * 2. * bj_PI / ua.balls))
                    SetUnitY(lbd.u, GetUnitY(ua.u) + BALL_OFF* Sin(facing + ua.phase + index * 2. * bj_PI / ua.balls))

                    lbd = lbd.next
                    index++


                // check for destruct
                let destroy_ref = ua
                ua = ua.next
                if not (destroy_ref.u.isAlive() and GetUnitTypeId(destroy_ref.u) > 0)

                    // destroy all balls
                    lbd = destroy_ref.ballHead
                    while lbd != null
                        swap = lbd.next
                        DestroyEffect(lbd.fx)
                        DummyUnitStack.release(lbd.u)
                        destroy lbd
                        lbd = swap


                    // destroy self, re-linking the list
                    if destroy_ref.prev != null
                        uaSwap = destroy_ref.prev
                        uaSwap.next = destroy_ref.next
                    else
                        beastHead = destroy_ref.next
                        if destroy_ref.next == null
                            PauseTimer(clock)


                    if destroy_ref.next != null
                        uaSwap = destroy_ref.next
                        uaSwap.prev = destroy_ref.prev

                    destroy destroy_ref


        static function initializeBeast(unit u)
            BeastLightningDat ua = new BeastLightningDat
            BeastLightningDat swap
            integer index = 0
            integer id = GetUnitTypeId(u)

            ua.typ = id

            ua.u = u
            ua.ticksUntilNextCheck = R2I(LIGHTNING_CHECK_PERIOD / CLOCK_PERIOD)
            ua.balls = 0

            while index < INITIAL_BALLS
                pushBall(u)
                index++


            fromBeast.saveInt(u.getHandleId(), ua castTo int)

            // push dat
            if beastHead == null
                beastHead = ua
                TimerStart(clock, CLOCK_PERIOD, true, function beastLightningTick)
            else
                swap = beastHead
                swap.prev = ua
                ua.next   = swap
                beastHead = ua



        static function initializeBeastEx(unit u, integer balls)
            BeastLightningDat ua = new BeastLightningDat
            BeastLightningDat swap
            integer index = 0
            integer id = GetUnitTypeId(u)

            ua.typ = id

            ua.u = u
            ua.ticksUntilNextCheck = R2I(LIGHTNING_CHECK_PERIOD / CLOCK_PERIOD)
            ua.balls = 0

            fromBeast.saveInt(u.getHandleId(), ua castTo int)

            // push dat
            if beastHead == null
                beastHead = ua
                TimerStart(clock, CLOCK_PERIOD, true, function beastLightningTick)
            else
                swap = beastHead
                swap.prev = ua
                ua.next   = swap
                beastHead = ua


            while index < balls
                pushBall(u)
                index++



        static function clearAll()
            unit iter
            integer index = 0

            // enumerate by player to catch decayed heroes
            while index <= Players.LAST_BEAST
                GroupEnumUnitsOfPlayer(grp, Players.fromId[index], null)
                while grp.hasNext()
                    iter = grp.next()
                    GroupRemoveUnit(grp, iter)
                    RemoveUnit(iter)

                index++



        private static function filterItems()
            item f = GetEnumItem()
            if GetItemTypeId(f) != ARTIFACT_ID
                RemoveItem(f)



        static function clearExtraItems()
            EnumItemsInRect(bj_mapInitialPlayableArea, null, function filterItems)


        static function clearNonHunters()
            unit iter
            integer index = 0

            // enumerate by player to catch decayed heroes
            while index <= Players.LAST_BEAST
                GroupEnumUnitsOfPlayer(grp, Players.fromId[index], null)
                while grp.hasNext()
                    iter = grp.next()
                    GroupRemoveUnit(grp, iter)

                    if GetUnitTypeId(iter) != HUNTER_ID or not iter.isAlive()
                        RemoveUnit(iter)


                index++



        static function endRoundVfx()
            unit iter

            // pause existing units
            GroupEnumUnitsInRect(grp, bj_mapInitialPlayableArea, null)
            while grp.hasNext()
                iter = grp.next()
                if GetPlayerId(GetOwningPlayer(iter)) <= Players.LAST_BEAST
                    PauseUnit(iter, true)
                    SetUnitInvulnerable(iter, true)

                GroupRemoveUnit(grp, iter)

    init
        integer index = 0

        // reset subHero array
        while index < Players.COUNT

            Units.subHero[index] = null

            index++
